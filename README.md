# FRCU-insumos



## Nombre
UTN FRCU Insumos

## Descripción
Aplicación simple para el control interno de solicitudes y retiro de insumos de oficina.

## Notas
Para utilizar este proyecto, luego de clonarlo se debe crear un archivo en el directorio "back"
con el nombre ".env" en el cuál se almacenará la información de conexión con la base de datos
con las siguientes variables de entorno: [DB_TYPE; DB_HOST; DB_PORT; DB_USERNAME; DB_PASSWORD; DB_DATABASE]

Ejemplo del archivo .env:
DB_TYPE=postgres
DB_HOST=localhost
DB_PORT=1234
DB_USERNAME=postgres
DB_PASSWORD=postgres
DB_DATABASE=database

## Estado actual
Etapas iniciales de desarrollo. ¡Hay mucho trabajo por hacer!

## Autor y Soporte
Desarrollado por Fernando Gomez Albornoz. 
