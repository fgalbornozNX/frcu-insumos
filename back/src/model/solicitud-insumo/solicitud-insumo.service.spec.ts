import { Test, TestingModule } from '@nestjs/testing';
import { SolicitudInsumoService } from './solicitud-insumo.service';

describe('SolicitudInsumoService', () => {
  let service: SolicitudInsumoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SolicitudInsumoService],
    }).compile();

    service = module.get<SolicitudInsumoService>(SolicitudInsumoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
