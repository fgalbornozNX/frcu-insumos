import { Injectable } from '@nestjs/common';
import { CreateSolicitudInsumoDto } from './dto/create-solicitud-insumo.dto';
import { UpdateSolicitudInsumoDto } from './dto/update-solicitud-insumo.dto';

@Injectable()
export class SolicitudInsumoService {
  create(createSolicitudInsumoDto: CreateSolicitudInsumoDto) {
    return 'This action adds a new solicitudInsumo';
  }

  findAll() {
    return `This action returns all solicitudInsumo`;
  }

  findOne(id: number) {
    return `This action returns a #${id} solicitudInsumo`;
  }

  update(id: number, updateSolicitudInsumoDto: UpdateSolicitudInsumoDto) {
    return `This action updates a #${id} solicitudInsumo`;
  }

  remove(id: number) {
    return `This action removes a #${id} solicitudInsumo`;
  }
}
