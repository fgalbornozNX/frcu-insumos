import { Module } from '@nestjs/common';
import { SolicitudInsumoService } from './solicitud-insumo.service';
import { SolicitudInsumoController } from './solicitud-insumo.controller';

@Module({
  controllers: [SolicitudInsumoController],
  providers: [SolicitudInsumoService],
})
export class SolicitudInsumoModule {}
