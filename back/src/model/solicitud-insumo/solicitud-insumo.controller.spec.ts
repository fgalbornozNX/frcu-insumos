import { Test, TestingModule } from '@nestjs/testing';
import { SolicitudInsumoController } from './solicitud-insumo.controller';
import { SolicitudInsumoService } from './solicitud-insumo.service';

describe('SolicitudInsumoController', () => {
  let controller: SolicitudInsumoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SolicitudInsumoController],
      providers: [SolicitudInsumoService],
    }).compile();

    controller = module.get<SolicitudInsumoController>(SolicitudInsumoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
