import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { SolicitudInsumoService } from './solicitud-insumo.service';
import { CreateSolicitudInsumoDto } from './dto/create-solicitud-insumo.dto';
import { UpdateSolicitudInsumoDto } from './dto/update-solicitud-insumo.dto';

@Controller('solicitud-insumo')
export class SolicitudInsumoController {
  constructor(private readonly solicitudInsumoService: SolicitudInsumoService) {}

  @Post()
  create(@Body() createSolicitudInsumoDto: CreateSolicitudInsumoDto) {
    return this.solicitudInsumoService.create(createSolicitudInsumoDto);
  }

  @Get()
  findAll() {
    return this.solicitudInsumoService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.solicitudInsumoService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSolicitudInsumoDto: UpdateSolicitudInsumoDto) {
    return this.solicitudInsumoService.update(+id, updateSolicitudInsumoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.solicitudInsumoService.remove(+id);
  }
}
