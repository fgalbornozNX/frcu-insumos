import { PartialType } from '@nestjs/mapped-types';
import { CreateSolicitudInsumoDto } from './create-solicitud-insumo.dto';

export class UpdateSolicitudInsumoDto extends PartialType(CreateSolicitudInsumoDto) {}
