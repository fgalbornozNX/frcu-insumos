import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Articulo {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

}
