import { BaseEntity } from "src/common/base.entity";
import { Entity, Column } from "typeorm";

export enum RolUsuario {
    SUPER       = "Supervisor",     // * Supervisor: Todos los permisos habilitados
    ADMIN       = "Administrador",  // * Administrador: puede gestionar todo, excepto crear nuevos usuarios
    USER        = "Usuario",        // * Usuario: sólo puede solicitar y ver su historial
}

@Entity('usuario')
export class Usuario extends BaseEntity {

    @Column({ length: 50 })
    nombre: string;  // * El nombre es único, pero se controla a nivel backend, no a nivel DB.

    @Column()
    password: string;

    @Column({
        type: 'enum',
        enum: RolUsuario,
        default: RolUsuario.USER,
    })
    rol: RolUsuario;

}