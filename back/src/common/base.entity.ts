import { CreateDateColumn, DeleteDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

export class BaseEntity {
    
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;

    // * Auditoría:

    @CreateDateColumn()
    fechaCreado!: Date;

    @UpdateDateColumn()
    fechaActualizado!: Date;

    @DeleteDateColumn()
    fechaEliminado?: Date;
    
}
