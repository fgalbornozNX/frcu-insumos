import { BaseEntity } from './base.entity';
import { BaseDTO } from './base.dto';

export abstract class AbstractMapper<T extends BaseEntity> {

    // * Devuelve un objeto conteniendo el ID de la entity recién creada
    public toID(entity: T): BaseDTO {
        return {
            id: entity.id,
        }
    }

    // * Respuesta estándar cuando se solicita una entity
    // ! Implementar en cada mapper
    public abstract toSimple(entity: T, loadFK?: boolean): BaseDTO | Promise<BaseDTO>;

    // * Respuesta completa sin datos de auditoría
    // ? Implementación opcional (override)
    public async toFull(entity: T): Promise<BaseDTO> {
        return await this.toSimple(entity, true);
    }

    public async toAudit(entity: T): Promise<BaseDTO> {
        const full = await this.toFull(entity);
        const activo = (entity.fechaEliminado === null);
        const result = {
            ...full,
            creado: entity.fechaCreado,
            actualizado: entity.fechaActualizado,
            activo: activo,
        }

        if(entity.fechaEliminado !== null) {
            const deleted = {
                ...result,
                eliminado: entity.fechaEliminado,
            }

            return deleted;
        }

        return result;
    }

    public async toListSimple(entities: T[], loadFK?: boolean): Promise<BaseDTO[]> {
        let response: BaseDTO[] = [];
        for(const e of entities) {
            response.push(await this.toSimple(e, loadFK));
        }

        return response;
    }

    public async toListFull(entities: T[]): Promise<BaseDTO[]> {
        let response: BaseDTO[] = [];
        for(const e of entities) {
            response.push(await this.toFull(e));
        }

        return response;
    }

    public async toListAudit(entities: T[]): Promise<BaseDTO[]> {
        let response: BaseDTO[] = [];
        for(const e of entities) {
            response.push(await this.toAudit(e));
        }

        return response;
    }

}