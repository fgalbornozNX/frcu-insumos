
/**
 * @example "Se lee?"
 */
export class PaginacionDTO {
    /**
     * @example 21
    */
    desde?: number;

    /**
     * @example 40
     */
    hasta?: number;
    // ? Se pueden agregar otras cosas, como el tipo de respuesta que se quiere (simple, full, auditoria)
    // ? Tambíen se puede setear el orden (ordenarPor, buscarPor). Hay un ejemplo en repos CB Back.
}