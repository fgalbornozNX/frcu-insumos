import { BaseEntity } from './base.entity';
import { Repository } from 'typeorm';
import { NotFoundException } from '@nestjs/common';
import { PaginacionDTO } from './paginacion.dto';

// Cuando se solicite algún método FIND, esto determina los skip/take por defecto.
const defaultDesde = 1;
const defaultHasta = 25; // Por defecto las búsquedas devuelven esta cantidad de elementos a menos que se especifique otra cosa.

export abstract class BaseService<T extends BaseEntity> {
  
  constructor(private repository: Repository<T>) {}

  // * Permite crear una nueva entity.
  abstract create(dto: any): Promise<T>;


  // * Permite actualizar una entity.
  abstract update(id: number, dto: any): Promise<T>;

  
  // * Devuelve todas las entities
  async findAll(withDeleted: boolean = false, relations?: string[], filtro?: PaginacionDTO) {
    if(filtro && filtro.desde && filtro.hasta) {
      const take = this.calcularTake(filtro);
      const skip = this.calcularSkip(filtro);
      
      if(relations && relations.length)
        return await this.repository.find({ withDeleted, relations, take, skip });
        
      return await this.repository.find({ withDeleted, take, skip });
    } 
    else {
      if(relations && relations.length)
        return await this.repository.find({ withDeleted, relations });
      
      return await this.repository.find({ withDeleted });
    }
  }


  // * Devuelve una entity. Busca por ID.
  async findOne(id: number, withDeleted: boolean = false, relations?: string[]) {
    if(relations) 
      return await this.repository.findOne(id, { withDeleted, relations });
  
    return await this.repository.findOne(id, { withDeleted });
  }

  
  // * Elimina una entity (Soft Delete).
  async remove(id: number) {
    const entity = await this.findOne(id);
    if(!entity)
      throw new NotFoundException('Entidad inexistente.');

    return await this.repository.softDelete(id);
  }

  // * Elimina una entity de forma permanente.
  async hardRemove(id: number) {
    const entity = await this.findOne(id, true);
    if(!entity)
      throw new NotFoundException('Entidad inexistente.');

    if(entity.fechaEliminado)
      return await this.repository.remove(entity);
    else
      return await this.remove(id);
  }

  protected async hardRemoveMany(entities: T[]) {
    for (const e of entities)
      await this.repository.remove(e);
  }

  // * Utils
  protected calcularTake(filtro: PaginacionDTO): number {
    if(!filtro || !filtro.hasta)
      return defaultHasta;

    let desde = 1; 

    if(filtro && filtro.desde && filtro.desde > 0)
      desde = filtro.desde;

    return ((filtro.hasta - desde) + 1) || defaultHasta;
  }

  protected calcularSkip(filtro: PaginacionDTO): number {
    let desde = defaultDesde;

    if(filtro && filtro.desde && filtro.desde > 0)
      desde = filtro.desde;

    return desde - 1;
  }
  
}
