import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { UsuarioModule } from './model/usuario/usuario.module';
import { SolicitudInsumoModule } from './model/solicitud-insumo/solicitud-insumo.module';
import { ArticuloModule } from './model/articulo/articulo.module';
import { RubroModule } from './model/rubro/rubro.module';
import { ProveedorModule } from './model/proveedor/proveedor.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { join } from 'path';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: process.env.DB_TYPE as 'postgres',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT, 10) || 5432,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      entities: [join(__dirname, '**', '*.entity.{ts,js}')],
      synchronize: true,
    }),
    UsuarioModule,
    SolicitudInsumoModule,
    ArticuloModule,
    RubroModule,
    ProveedorModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
